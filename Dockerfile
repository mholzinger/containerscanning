# Use a base image that includes both Python and Node.js
FROM nikolaik/python-nodejs:latest

# Set the working directory in the container
WORKDIR /app

# Copy the Python requirements file
COPY requirements.txt /app/

# Install Python dependencies
RUN pip install -r requirements.txt

# Copy the NPM package.json file
COPY package.json /app/

# Install NPM dependencies
RUN npm install

# Copy the Flask application
COPY . /app

# Run the application
CMD ["python", "app.py"]
