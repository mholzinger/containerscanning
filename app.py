from flask import Flask
import platform
import subprocess

app = Flask(__name__)

@app.route('/')
def index():
    python_version = platform.python_version()
    linux_version = subprocess.check_output("cat /etc/os-release", shell=False).decode()
    return f"Python Version: {python_version}\nLinux Version:\n{linux_version}"

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=False)
