# containerscanning

This project is a basic python web server which introduces a node dependency and enables SAST container scanning for Gitlab CI.


The scope of this example is to add a common node dependency with known a vulnerability posture, `lodash` and utilize the vulnerability dashboard to illustrate SAST, container and dependancy scanning.

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/cipherpop-guidance/containerscanning/-/settings/integrations)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

